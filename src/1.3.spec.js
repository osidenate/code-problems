// URLify: Write a method to replace all spaces in a string with '%20'. You may assume that the string has sufficient
// space at the end to hold the additional characters, and that you are given the "true" length of the string. (Note: If
// implementing in Java, please use a character array so that you can perform this operation in place.)
const { expect } = require('chai');
const { replaceSpaces } = require('./1.3.js');

describe('Replace spaces', () => {
  it('should replace spaces with %20', () => {
    expect(replaceSpaces('XX XX')).to.equal('XX%20XX');
    expect(replaceSpaces('XX  XX')).to.equal('XX%20%20XX');
    expect(replaceSpaces(' XXXX ')).to.equal('%20XXXX%20');
  });
});
