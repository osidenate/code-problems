// Check if one string is contained within another string

const contains = (containerStr, searchStr) => {
  const container = Array.from(containerStr);
  const search = Array.from(searchStr);

  outter: // eslint-disable-line
  for (let i = 0; i <= container.length - search.length; i++) {
    if (container[i] === search[0]) {
      for (let g = 0; g < search.length; g++) {
        const containerIndex = i + g;
        if (container[containerIndex] !== search[g]) {
          continue outter;
        }
        if (g === search.length - 1) {
          return true;
        }
      }
    }
  }

  return false;
};

const checkContains = (str1, str2) => {
  if (str1.length === str2.length) {
    return str1 === str2;
  }

  if (str1.length > str2.length) {
    return contains(str1, str2);
  }

  return contains(str2, str1);
};

module.exports = {
  checkContains,
};
