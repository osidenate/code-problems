// Is Unique: Implement an algorithm to determine if a string has all unique characters.
// What if you cannot use additional data structures?

const isUnique = (str) => {
  const characters = Array.from(str);

  return !characters.some((char1, index1) => {
    return characters.some((char2, index2) => {
      const foundDuplicate = char1 === char2 && index1 !== index2;
      return foundDuplicate;
    });
  });
};

const isUnique2 = (str) => {
  const characters = Array.from(str);
  const length = characters.length;

  return !characters.some((char1, index1) => {
    for (let index2 = index1 + 1; index2 < length; index2++) {
      const foundDuplicate = char1 === characters[index2] && index1 !== index2;
      if (foundDuplicate) {
        return true;
      }
    }
  });
};

// uses hash map
const isUnique3 = (str) => {
  const characterMap = {};
  const characters = Array.from(str);

  for (let i = 0; i < characters.length; i++) {
    const char = characters[i];
    if (characterMap[char]) {
      return false;
    }

    characterMap[char] = true;
  }

  return true;
};

module.exports = {
  isUnique,
  isUnique2,
  isUnique3,
};
