// Check if one string is contained within another string

const { expect } = require('chai');
const { checkContains } = require('./1.2b');

describe('1.2: Determine if one string is contained in the other', () => {
  it('Implementation 1', () => {
    // Contained

    let str1 = 'abc';
    let str2 = 'abcdefg';
    expect(checkContains(str1, str2)).to.be.true;
    expect(checkContains(str2, str1)).to.be.true;

    str1 = 'breakable';
    str2 = 'unbreakable';
    expect(checkContains(str1, str2)).to.be.true;

    str1 = 'banana';
    str2 = 'yo, banana boy';
    expect(checkContains(str1, str2)).to.be.true;

    // Not Contained

    str1 = 'qwe';
    str2 = 'asd';
    expect(checkContains(str1, str2)).to.be.false;

    str1 = 'red';
    str2 = 'green';
    expect(checkContains(str1, str2)).to.be.false;

    str1 = 'dog';
    str2 = 'donut';
    expect(checkContains(str1, str2)).to.be.false;
  });
});
