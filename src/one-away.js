// One Away
// insert, remove, replace only one time

const isOneAway = (str1, str2) => {
  const originalStr1 = [...str1];
  const originalStr2 = [...str2];

  const tried = {
    insert: false,
    remove: false,
    replace: false,
  };
  let firstContentionIndex = null;

  const maxLength = Math.max(str1.length, str2.length);
  for (let i = 0; i < maxLength; i++) {
    if (str1[i] === str2[i]) {
      continue;
    } else if (firstContentionIndex !== null) {
      if (tried.insert && tried.remove && tried.replace) {
        return false;
      }
      str1 = [...originalStr1];
      str2 = [...originalStr2];
      i = firstContentionIndex - 1;
      firstContentionIndex = null;
    } else {
      if (!tried.insert) {
        tried.insert = true;
        if (str1.length < str2.length) {
          str1.splice(i, 0, str2[i]);
        } else {
          i--;
          continue;
        }
      } else if (!tried.remove) {
        tried.remove = true;
        if (str1.length > str2.length) {
          str1.splice(i, 1);
        } else {
          i--;
          continue;
        }
      } else if (!tried.replace) {
        tried.replace = true;
        str1[i] = str2[i];
      }

      firstContentionIndex = i;
    }
  }

  return str1.length === str2.length;
};

module.exports = {
  isOneAway,
};
