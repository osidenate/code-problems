const findPalindrome = (str) => {
  // lowercase all characters
  str = str.toLowerCase();

  // remove all spaces
  str = str.replace(/\s/g, '');

  // push each character in str to a map
  const charMap = [...str].reduce((charMap, char) => {
    const count = charMap.get(char);
    count ? charMap.set(char, count + 1) : charMap.set(char, 1);
    return charMap;
  }, new Map());

  let pivot = '';
  charMap.forEach((numChars, letter) => {
    const isOdd = !(numChars % 2);
    if (isOdd) {
      if (pivot !== '') {
        pivot = -1;
      } else {
        pivot = letter;
        charMap.set(letter, numChars - 1);
      }
    }
  });

  if (pivot === -1) {
    return null;
  }

  let result = '';
  const stack = [];

  charMap.forEach((numChars, letter) => {
    for (let i = 0; i < numChars / 2; i++) {
      result += letter;
      stack.push(letter);
    }
  });

  result += pivot;

  for (let i = 0; i < stack.length; i++) {
    result += stack[i];
  }

  return result;
};

const isPalindrome = (str) => {
  // lowercase all characters
  str = str.toLowerCase();

  // remove all spaces
  str = str.replace(/\s/g, '');

  // push each character in str to a map
  const charMap = [...str].reduce((charMap, char) => {
    const count = charMap.get(char);
    count ? charMap.set(char, count + 1) : charMap.set(char, 1);
    return charMap;
  }, new Map());

  let numOfOddCharacters = 0;
  charMap.forEach(numChars => {
    numOfOddCharacters += numChars % 2;
  });

  return numOfOddCharacters <= 1;
};

module.exports = {
  isPalindrome,
};

console.log(findPalindrome(''));
console.log(findPalindrome('123456'));
