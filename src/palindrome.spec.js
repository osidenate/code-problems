const { expect } = require('chai');
const { isPalindrome } = require('./palindrome');

describe('Palindrom Permutations', () => {
  it('should detect if the string is a palindrome', () => {
    expect(isPalindrome('tactca')).to.equal(true);
    expect(isPalindrome('tactcoa')).to.equal(true);
    expect(isPalindrome('tact coa')).to.equal(true);
    expect(isPalindrome('Tact Coa')).to.equal(true);

    expect(isPalindrome('qwe')).to.equal(false);
    expect(isPalindrome('qqwee')).to.equal(false);
  });
  xit('should be able to find a palidrome', () => {
    let findPalindrome = () => {};
    expect(findPalindrome('qwe')).to.equal(null);
  });
});
