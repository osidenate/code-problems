// Is Unique: Implement an algorithm to determine if a string has all unique characters.
// What if you cannot use additional data structures?

const { expect } = require('chai');
const { isUnique, isUnique2, isUnique3 } = require('./1.1');

describe('1.1: Determine if a string has all unique characters', () => {
  const testUnique = (uniqueFunc) => {
    const str1 = '';
    const str2 = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()_+-=';
    expect(uniqueFunc(str1)).to.be.true;
    expect(uniqueFunc(str2)).to.be.true;

    const str3 = 'aa';
    const str4 = 'abccd';
    const str5 = '  ';
    const str6 = '~abc~';
    expect(uniqueFunc(str3)).to.be.false;
    expect(uniqueFunc(str4)).to.be.false;
    expect(uniqueFunc(str5)).to.be.false;
    expect(uniqueFunc(str6)).to.be.false;
  };

  it('Implementation 1', () => {
    testUnique(isUnique);
  });

  it('Implementation 2', () => {
    testUnique(isUnique2);
  });

  it('Implementation 3', () => {
    testUnique(isUnique3);
  });
});
