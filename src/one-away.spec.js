// Check if one string is contained within another string

const { isOneAway } = require('./one-away');
const { expect } = require('chai');

describe('Check if a string is one operation away from another string', () => {
  it('One operation away', () => {
    let arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    let arr2 = ['q', 'w', 'e', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'strings match exactly').to.be.true;

    // insert

    arr1 = ['@', 'q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', 'e', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'extra char at start of string').to.be.true;

    arr1 = ['q', 'e', 'r', 't', 'y'];
    arr2 = ['q', '@', 'e', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'extra char in the middle of string').to.be.true;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', 'e', 'r', 't', 'y', '@'];
    expect(isOneAway(arr1, arr2), 'extra char at end of string').to.be.true;

    // remove

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['w', 'e', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'remove first char').to.be.true;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', 'e', 'r', 'y'];
    expect(isOneAway(arr1, arr2), 'remove char in the middle').to.be.true;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', 'e', 'r', 't'];
    expect(isOneAway(arr1, arr2), 'remove last char').to.be.true;

    // replace

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['@', 'w', 'e', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'replace first char').to.be.true;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', '@', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'replace char in the middle').to.be.true;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', 'e', 'r', 't', '@'];
    expect(isOneAway(arr1, arr2), 'replace last char').to.be.true;

    // negatives

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'w', 'e', '@', '@', 'r', 't', 'y'];
    expect(isOneAway(arr1, arr2), 'insert 2 chars').to.be.false;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['@', 'q', 'w', 'e', 'r', 't', 'y', '@'];
    expect(isOneAway(arr1, arr2), 'insert 2 chars pt.2').to.be.false;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['@', 'w', 'e', 'r', '@', 'y'];
    expect(isOneAway(arr1, arr2), 'replace 2 chars').to.be.false;

    arr1 = ['q', 'w', 'e', 'r', 't', 'y'];
    arr2 = ['q', 'e', 'r', 't'];
    expect(isOneAway(arr1, arr2), 'remove 2 chars').to.be.false;
  });
});
