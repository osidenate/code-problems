// UVa 11496
// [1, -3] -> 2
// [40, 0, -41, 0, 41, 42] -> 2
// [200, 450, 449, 450] -> 4

const isPeak = (a1, a2, a3) => {
  if (a1 === a2) {
    return 0;
  }
  const direction = a1 < a2 ? 'up' : 'down';
  if (direction === 'up' && a2 > a3) {
    return 1;
  } else if (direction === 'down' && a2 < a3) {
    return 1;
  }
  return 0;
};

const countPeaks = (pcm) => {
  let numOfPeaks = 0;
  const last = pcm[pcm.length - 1];
  numOfPeaks += isPeak(last, pcm[0], pcm[1]);

  for (let i = 1; i < pcm.length; i++) {
    let a3;
    if (pcm.length === i + 1) {
      a3 = pcm[0];
    } else {
      a3 = pcm[i + 1];
    }

    numOfPeaks += isPeak(pcm[i - 1], pcm[i], a3);
  }

  return numOfPeaks;
};

console.log(countPeaks([1, -3]));
console.log(countPeaks([40, 0, -41, 0, 41, 42]));
console.log(countPeaks([200, 450, 449, 450]));
